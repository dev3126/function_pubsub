FROM python:3.8
RUN mkdir app
COPY app/* app/
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requiriments.txt
CMD ["python","work.py"]