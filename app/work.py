import tweepy
import requests
from config import *


def PegaPreco():
    url = 'https://openexchangerates.org/api/latest.json?app_id=7d416927da7341ae98914f7bff6b4abc&show_alternative=1&symbols=BTC'
    dolar = requests.get(url)
    dolar = dolar.json()['rates']['BTC']
    dolar = 1/dolar
    dolar = "{:.2f}".format(dolar)
    return dolar

client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)

# Create Tweet

# The app and the corresponding credentials must have the Write permission

# Check the App permissions section of the Settings tab of your app, under the
# Twitter Developer Portal Projects & Apps page at
# https://developer.twitter.com/en/portal/projects-and-apps

# Make sure to reauthorize your app / regenerate your access token and secret 
# after setting the Write permission
print('consumer_key {}'.format(consumer_key))
dolar = PegaPreco()

response = client.create_tweet(
    text="O preço do BTC, hoje, em dolar, é.: {}".format(dolar)
)
print(f"https://twitter.com/user/status/{response.data['id']}")