version:=$(shell cat version)
project=rodrigorootrj/dev-pubsub
TAG=${project}:${version}
# STRING_FILE :="template/Chart/values"
##Inserindo Variaveis no Makefile.
var ?= keys/env.template
include $(var)
export $(shell sed 's/=.*//' $(var))

.PHONY: run_variables run
echo:
	@echo  ${TAG}
build:
	@docker build . -t ${TAG}
run: build
	@docker run -it -e consumer_key=${consumer_key} -e consumer_secret=${consumer_secret} -e access_token=${access_token} \
	-e access_token_secret=${access_token_secret} ${TAG}
#run, com variaveis de ambiente.
run_variables: build
	@docker run -it -v $$PWD/app/:/app  -v $$PWD/keys/:/keys  -e consumer_key=${consumer_key} -e consumer_secret=${consumer_secret} /
	-e access_token=${access_token} -e access_token_secret=${access_token_secret} ${TAG}	
push: build
	@docker push ${TAG}
#ts docker hub
docker_remove:
	@docker rmi ${TAG} --force
docker_run_hub:
	@docker run -it ${TAG}	